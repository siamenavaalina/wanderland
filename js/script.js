$('.block5__slider').slick({
	slidesToShow: 3,
	slidesToScroll: 1,
	autoplay: true,
	autoplaySpeed: 2000,
	arrows: false,
	 responsive: [
    {
      breakpoint: 1025,
      settings: {
        slidesToShow: 2,
        
        
      }
    },
    {
      breakpoint: 681,
      settings: {
        slidesToShow: 1,
        
       
      }
    },
    {
      breakpoint: 320,
      settings: {
        slidesToShow: 1,
        
      }
    }
   
  ]
});
$('.block7_single_slider').slick({
	slidesToShow: 1,
	slidesToScroll: 1,
	autoplay: true,
	autoplaySpeed: 2000,
	infinite: true,
	arrows: false
});
